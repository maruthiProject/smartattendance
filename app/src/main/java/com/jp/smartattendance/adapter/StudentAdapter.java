package com.jp.smartattendance.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jp.smartattendance.R;
import com.jp.smartattendance.pojo.StudentListModel;

import java.util.List;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.StudentModel> {

    Context context;
    List<StudentListModel> studentListModelList;
    ItemListener listener;
    public interface ItemListener {
       void onclickUpdate(StudentListModel studentListModelList);
    }

    public StudentAdapter(Context context, List<StudentListModel> studentListModelList,ItemListener listener) {
        this.context = context;
        this.listener = listener;
        this.studentListModelList = studentListModelList;
    }

    @NonNull
    @Override
    public StudentModel onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.item_student_list,null);
        return new StudentModel(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StudentModel holder, int position) {
    final StudentListModel studentListModel=studentListModelList.get(position);
        holder.name.setText(studentListModel.getName());
        holder.mobileNo.setText(studentListModel.getMobileNo());
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onclickUpdate(studentListModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return studentListModelList.size();
    }

    class StudentModel extends RecyclerView.ViewHolder {

        TextView name,mobileNo;
        RelativeLayout relativeLayout;
        StudentModel(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
            mobileNo=itemView.findViewById(R.id.moileNo);
            relativeLayout=itemView.findViewById(R.id.relativeLayout);
        }
    }
}
