package com.jp.smartattendance.fragment;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.jp.smartattendance.R;
import com.jp.smartattendance.adapter.StudentAdapter;
import com.jp.smartattendance.helper.StaticData;
import com.jp.smartattendance.pojo.StudentListModel;

import java.util.ArrayList;
import java.util.List;

public class StudentRegFragment extends Fragment implements StudentAdapter.ItemListener, LocationListener{
    View view;
  FirebaseFirestore firebaseFirestore;
    FirebaseDatabase database;
    DatabaseReference myRef;
    StaticData staticData;
    StudentAdapter studentAdapter;
    RecyclerView recyclerView;
    LocationManager locationManager;
    String provider;
    List<StudentListModel> studentListModelArrayList =new ArrayList<>();
    private String TAG=StudentRegFragment.class.getSimpleName();
    private String latLong;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_student_reg, container, false);
        staticData=new StaticData(getActivity());
        recyclerView=view.findViewById(R.id.studentList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        //firebase
        initFirebase();
        return view;
    }

    private void initFirebase() {
        FirebaseApp.initializeApp(getActivity());
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
        //firebase fire storage this data stored in firestorage.
        //getList();
        addListtoFirebase();
        getListToFirebase();
    }

   private void addListtoFirebase(){
        myRef.child("studentList").removeValue();
        studentListModelArrayList=staticData.getStudentList();
        for (StudentListModel studentListModel:studentListModelArrayList){
            myRef.child("studentList").child(studentListModel.getId()).setValue(studentListModel);
        }
    }

    private void getListToFirebase() {
        myRef.child("studentList").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.e(TAG, "onDataChange: "+dataSnapshot.getChildren() );
                studentListModelArrayList.clear();
                for (DataSnapshot postSnapshot:dataSnapshot.getChildren()){
//                    StudentListModel studentListModel =postSnapshot.getValue(StudentListModel.class);
//                    studentListModelArrayList.add(studentListModel);
//                    staticData.getStudentList();
                    String name = postSnapshot.child("name").getValue(String.class);
                    String mobileNo = postSnapshot.child("mobileNo").getValue(String.class);
                    String id = postSnapshot.child("id").getValue(String.class);
                    String email= postSnapshot.child("email").getValue(String.class);
                    String classId= postSnapshot.child("classId").getValue(String.class);
                    String classes= postSnapshot.child("classes").getValue(String.class);
                    String latLong= postSnapshot.child("latLong").getValue(String.class);
                    studentListModelArrayList.add(new StudentListModel(id,name,classId,classes,mobileNo,email,latLong));
                    Log.e("TAG", "name: "+name );
                }
                studentAdapter=new StudentAdapter(getActivity(),studentListModelArrayList,StudentRegFragment.this);
                recyclerView.setAdapter(studentAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: "+databaseError.getMessage() );
            }
        });
    }
    private void getList(){
        firebaseFirestore=FirebaseFirestore.getInstance();
        firebaseFirestore.collection("studentList").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                if (e!=null){
                    Log.e(TAG, "Error: "+e.getMessage() );
                }
//                for (DocumentSnapshot documentSnapshots: queryDocumentSnapshots){
//
//                    String mobileNo=documentSnapshots.getString("mobileNo");
//                    Log.e(TAG, "onEvent: mobileNo : "+documentSnapshots );
//                }
                for (DocumentChange documentSnapshots: queryDocumentSnapshots.getDocumentChanges()){
                    if (documentSnapshots.getType().equals(DocumentChange.Type.ADDED)){
//                        StudentListModel studentListModel=documentSnapshots.getDocument().toObject(StudentListModel.class);
                        String mobileNo=documentSnapshots.getDocument().getString("mobileNo");
                        String name=documentSnapshots.getDocument().getString("name");
                        String id=documentSnapshots.getDocument().getString("id");
                        String email=documentSnapshots.getDocument().getString("email");
                        String classId=documentSnapshots.getDocument().getString("classId");
                        String classes=documentSnapshots.getDocument().getString("classes");
                        String latLong=documentSnapshots.getDocument().getString("latLong");
                        studentListModelArrayList.add(new StudentListModel(id,name,classId,classes,mobileNo,email,latLong));
                    }
                }
                studentAdapter=new StudentAdapter(getActivity(),studentListModelArrayList,StudentRegFragment.this);
                recyclerView.setAdapter(studentAdapter);
            }
        });

            }

    @Override
    public void onclickUpdate(StudentListModel studentListModelList) {
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);

        if (provider != null && !provider.equals("")) {
            Location location = locationManager.getLastKnownLocation(provider);

            locationManager.requestLocationUpdates(provider, 2000, 1, this);
            locationManager.getLastKnownLocation(provider);
            latLong=location.getLatitude()+" "+location.getLongitude();
            Log.e(TAG, "onclickUpdate: latLong : "+ location.getLatitude()+" "+location.getLongitude());


//            if (location != null)
////                onLocationChanged(location);
//            else
//                Toast.makeText(getBaseContext(), "Location can't be retrieved",
//                        Toast.LENGTH_SHORT).show();

        } else {
//            Toast.makeText(getBaseContext(), "No Provider Found",
//                    Toast.LENGTH_SHORT).show();
        }
        updateList(studentListModelList,latLong);
    }

    private void updateList(StudentListModel studentListModelList,String latLong) {

        Log.e(TAG, "updateList: "+studentListModelList.getId() );
        DatabaseReference reference =FirebaseDatabase.getInstance().getReference("studentList").child(studentListModelList.getId());
//        StudentListModel studentListModel=new StudentListModel(id, "maruthi","9944614823","","","",latLong);
//        reference.setValue(studentListModel);
        StudentListModel studentListModel=new StudentListModel(studentListModelList.getId(), studentListModelList.getName(),studentListModelList.getClassId(),studentListModelList.getClasses(),
                studentListModelList.getMobileNo(),studentListModelList.getEmail(),latLong);
        reference.setValue(studentListModel);
        Toast.makeText(getActivity(),"Update successfully" ,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
