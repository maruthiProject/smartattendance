package com.jp.smartattendance.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.jp.smartattendance.R;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;

public class UploadFileFCStorage extends Fragment implements View.OnClickListener {
    View view;
    ImageView finerPrint;
    TextView choose, upload,goToMap,tapToCheck;
    int PICK_IMAGE = 234;
    private StorageReference mStorageRef;
    Uri filePath;
    private String TAG=UploadFileFCStorage.class.getSimpleName();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_upload_file_storage, container, false);
        finerPrint = view.findViewById(R.id.fingerprint);
        choose = view.findViewById(R.id.choose);
        upload = view.findViewById(R.id.upload);
        goToMap = view.findViewById(R.id.gotomap);
        tapToCheck = view.findViewById(R.id.tapToCheck);
        choose.setOnClickListener(this);
        upload.setOnClickListener(this);
        goToMap.setOnClickListener(this);
        tapToCheck.setOnClickListener(this);
        FirebaseApp.initializeApp(getActivity());
        mStorageRef = FirebaseStorage.getInstance().getReference();
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.choose:
                choose();
                break;
            case R.id.upload:
//                upload();
                uploadByteImg();
                break;
            case R.id.gotomap:
                startActivity(new Intent(getContext(), ViewMapsActivity.class));
                break;
            case R.id.tapToCheck:
              setFragment(new StudentRegFragment());
                break;
        }
    }

    private void setFragment(Fragment fragment) {
        // create a FragmentManager
        FragmentManager fm = getActivity().getSupportFragmentManager();
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.framLayout, fragment);
        fragmentTransaction.commit(); // save the changes
    }

    private void choose() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
       this.startActivityForResult(Intent.createChooser(intent, "Select file"), PICK_IMAGE);
    }

    private void upload() {
        Log.e(TAG, "upload: "+filePath );
        if(filePath!=null) {
            final ProgressDialog dialog = new ProgressDialog(getActivity());
            dialog.setTitle(R.string.uploading);
            dialog.show();
            StorageReference riversRef = mStorageRef.child("fingerPrintImg/fingerPrint.jpg");

            riversRef.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Log.e(TAG, "onSuccess: " );
                            dialog.dismiss();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            // Handle unsuccessful uploads
                            Log.e(TAG, "onFailure: " );
                            dialog.dismiss();
                            // ...
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                            dialog.setMessage(((int) progress) + "% " + R.string.uploading);
                        }
                    });
        }else {
            Log.e(TAG, "upload: null file path" );
        }
    }

    private void uploadByteImg(){

// Create a reference to "mountains.jpg"
        StorageReference mountainsRef = mStorageRef.child("fingerPrintImg/mountains.jpg");

// Create a reference to 'images/mountains.jpg'
        StorageReference mountainImagesRef = mStorageRef.child("fingerPrintImg/mountains.jpg");

// While the file names are the same, the references point to different files
//        mountainsRef.getName().equals(mountainImagesRef.getName());    // true
//        mountainsRef.getPath().equals(mountainImagesRef.getPath());    // false
// Get the data from an ImageView as bytes
        finerPrint.setDrawingCacheEnabled(true);
        finerPrint.buildDrawingCache();
        Bitmap bitmap = ((BitmapDrawable) finerPrint.getDrawable()).getBitmap();
        if (bitmap!=null) {
            final ProgressDialog dialog = new ProgressDialog(getActivity());
            dialog.setTitle(R.string.uploading);
            dialog.show();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] data = baos.toByteArray();

            UploadTask uploadTask = mountainsRef.putBytes(data);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e(TAG, "onFailure: " );
                    dialog.dismiss();
                }
            })
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Log.e(TAG, "onSuccess: " );
                            dialog.dismiss();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                            dialog.setMessage(((int) progress) + "% " + R.string.uploading);
                        }
                    });
        }else {
            Log.e(TAG, "uploadByteImg: null bitmap " );
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE) {
            try {
                if (data.getData() != null) {
                    filePath = data.getData();
                    if (filePath != null) {
                        InputStream imageStream = Objects.requireNonNull(getActivity()).getContentResolver().openInputStream(Objects.requireNonNull(filePath));
                        Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        finerPrint.setImageBitmap(selectedImage);
                        //  imageViewUserImage.setImageBitmap(selectedImage);
                    }
                }
            } catch (FileNotFoundException | NullPointerException e) {
                e.printStackTrace();
            }
        }
    }
}
