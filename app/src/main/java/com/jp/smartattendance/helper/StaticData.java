package com.jp.smartattendance.helper;

import android.content.Context;

import com.jp.smartattendance.pojo.StudentListModel;

import java.util.ArrayList;
import java.util.List;

public class StaticData {
    Context context;

    public StaticData(Context context) {
        this.context = context;
    }

    public List<StudentListModel> getStudentList(){
        List<StudentListModel> studentListModelArrayList=new ArrayList<>();
        studentListModelArrayList.add(new StudentListModel("1","raja","01","CSC","9944614823","maruthiraja15di20@gmail.com",""));
        studentListModelArrayList.add(new StudentListModel("2","jaya prathap","02","IT","7871076570","maruthiraja15di20@gmail.com",""));
        return studentListModelArrayList;
    }
}
