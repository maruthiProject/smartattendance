package com.jp.smartattendance.pojo;

public class StudentListModel {
    private String id, name, classId, classes,mobileNo,email,latLong;

    public StudentListModel(String id, String name, String classId, String classes, String mobileNo, String email, String latLong) {
        this.id = id;
        this.name = name;
        this.classId = classId;
        this.classes = classes;
        this.mobileNo = mobileNo;
        this.email = email;
        this.latLong = latLong;
    }

    public StudentListModel(String id, String name, String mobileNo) {
        this.id = id;
        this.name = name;
        this.mobileNo = mobileNo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClasses() {
        return classes;
    }

    public void setClasses(String classes) {
        this.classes = classes;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLatLong() {
        return latLong;
    }

    public void setLatLong(String latLong) {
        this.latLong = latLong;
    }
}
